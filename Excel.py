# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'C:\Users\33670\Desktop\untitled.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit, QFileDialog

import os.path
import sys

class Ui_Form(QWidget):

    def saved(self):
        text = self.textzone.toPlainText()
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()","","All Files (*);;Text Files (*.txt)", options=options)
        file_exists = os.path.isfile(fileName)
        if file_exists:
            os.remove(fileName)
            f = open(fileName, "a")
            f.write(text)
            f.close()
        else:
            f = open(fileName, "a")
            f.write(text)
            f.close()
        print("saved")

    def loaded(self,txt):
        f = open(txt, "r")
        text = f.read()
        f.close()
        self.textzone.setPlainText(text)

    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All Files (*);;Python Files (*.py)", options=options)
        if fileName:
            self.loaded(fileName)

    def colorred(self):
        self.textzone.setStyleSheet("QTextEdit {color:red}")


    def setupUi(self, Form):

        Form.setObjectName("Form")
        Form.resize(1024, 768)
        Form.setMinimumSize(QtCore.QSize(1024, 768))
        self.verticalLayout = QtWidgets.QVBoxLayout(Form)
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.load = QtWidgets.QPushButton(Form)
        self.load.setObjectName("load")
        self.horizontalLayout.addWidget(self.load)
        self.save = QtWidgets.QPushButton(Form)
        self.save.setObjectName("save")
        self.horizontalLayout.addWidget(self.save)
        self.red = QtWidgets.QPushButton(Form)
        self.red.setObjectName("red")
        self.horizontalLayout.addWidget(self.red)

        self.black = QtWidgets.QPushButton(Form)
        self.black.setObjectName("black")
        self.horizontalLayout.addWidget(self.black)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem = QtWidgets.QSpacerItem(20, 10, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Fixed)
        self.verticalLayout.addItem(spacerItem)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.textzone = QtWidgets.QTextEdit(Form)
        self.textzone.setMinimumSize(QtCore.QSize(1000, 0))
        self.textzone.setObjectName("textzone")
        self.horizontalLayout_2.addWidget(self.textzone)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        spacerItem3 = QtWidgets.QSpacerItem(1, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

        self.save.clicked.connect(self.saved)
        self.load.clicked.connect(self.openFileNameDialog)
        self.red.clicked.connect(self.colorred)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form", "Form"))
        self.load.setText(_translate("Form", "Load"))
        self.save.setText(_translate("Form", "Save"))
        self.red.setText(_translate("Form", "Red"))
        self.black.setText(_translate("Form", "Black"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Form()
    ui.setupUi(Form)
    Form.show()
    sys.exit(app.exec_())
